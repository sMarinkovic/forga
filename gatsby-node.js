
exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html" || stage === "develop-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /bad-module/,
            use: loaders.null(),
          },
          {
            test: /react-owl-carousel/,
            use: loaders.null(),
          },
          {
            test: /react-animated-cursor/,
            use: loaders.null(),
          },
          {
            test:/aos/,
            use: loaders.null(),
          },
          {
            test:/gatsby-plugin-express/,
            use: loaders.null(),
          }
        ],
      },
    })
  }
}
