/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 */

// You can delete this file if you're not using it
import "@fontsource/inter"

export const onInitialClientRender = () => {
  
  if (typeof window === 'undefined') console.log('Window is not there')
}