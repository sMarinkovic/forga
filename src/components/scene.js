import * as THREE from 'three'
import React, { Suspense, useEffect, useState, useRef } from 'react'
import { useFrame, useThree } from '@react-three/fiber'
import { PerspectiveCamera,  MeshDistortMaterial, Environment, ContactShadows} from '@react-three/drei'
import { useSpring } from '@react-spring/core'
import { a } from '@react-spring/three'

const AnimatedMaterial = a(MeshDistortMaterial)

export default function Scene({ setBg }) {
  const sphere = useRef()
  const light = useRef()

  const [mode, setMode] = useState(false)
  const [down, setDown] = useState(false)
  const [hovered, setHovered] = useState(false)
  const [offset, setOffset] = useState(0)


  useFrame((state) => {


    light.current.position.x = state.mouse.x * 30
    light.current.position.y = state.mouse.y * 30

    if (sphere.current) {

      sphere.current.position.x = THREE.MathUtils.lerp(sphere.current.position.x, hovered ? state.mouse.x / 2 : 0, 0.2)
      sphere.current.position.y = THREE.MathUtils.lerp(
        sphere.current.position.y,
        Math.sin(state.clock.elapsedTime / 1.5) / 6 + (hovered ? state.mouse.y / 2 : 0), 1
      )
    }
  })


  useEffect(() => {
      const onScroll = () => setOffset(window.pageYOffset);
      window.removeEventListener('scroll', onScroll);
      if(window !== "undefined") {
      window.addEventListener('scroll', onScroll, { passive: true });
      return () => window.removeEventListener('scroll', onScroll);
      }
  }, []);


  const [{ wobble, coat, color, ambient, env}] = useSpring(
    {
      wobble: down ? 0.8 : hovered ? 1 : 0.9,
    //   coat: mode && !hovered ? 0.2 : 0.8,
      ambient: mode && !hovered ? 5.5 : 1.1,
      env: hovered ? 0.8 : 1,
      // color: hovered ? '#00A38D' :  '#7C44F1',
      color:  '#7C44F1',
      config: (n) => n ===  hovered && { mass: 3, tension: -12, friction: 1 }
    },
    [hovered, down, offset]
  )

  return (
    <>
      <PerspectiveCamera makeDefault position={[1, offset/2000, 4]} fov={35} >
        {/* <a.directionalLight ref={lightOpposite} intensity={env} color="#259B69" /> */}
        <a.ambientLight intensity={ambient}  color="#00A38D"  />
        <a.pointLight ref={light} position-z={-2} intensity={env} color="#D1503F" />
      </PerspectiveCamera>

      <Suspense fallback={null}>
        <a.mesh
          ref={sphere}
          // scale={wobble}
          scale={[-1,-1,1-offset/19000]}
          rotation={[11, offset/7000, 51]}
          onPointerOver={() => setHovered(true)}
          onPointerOut={() => setHovered(false)}
          onPointerDown={() => setDown(true)}
          // onPointerUp={() => { setDown(false)
          //   setMode(!mode)
          //   setBg({ background: !mode ? '#202020' : '#f0f0f0', fill: !mode ? '#f0f0f0' : '#202020' })
          // }}
          >
          <sphereBufferGeometry  position={[1, -1, 20]} args={[1,  60, 80]} />
          <AnimatedMaterial  color={color} envMapIntensity={env} clearcoat={coat} clearcoatRoughness={offset/200} metalness={offset/600} />
        </a.mesh>
        <Environment preset="warehouse" />
        <ContactShadows
          rotation={[Math.PI / 2, 0, 0]}
          position={[0, -1.1, 0]}
          opacity={.9}
          width={15}
          height={15}
          blur={2.5}
          far={.6}
        />
      </Suspense>
    </>
  )
}
