/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import * as React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import AnimatedCursor from "react-animated-cursor"
import Header from "./header"
import "./layout.css"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  
  return (
    <>
    <div className="cursor">
      <AnimatedCursor
      innerSize={8}
      outerSize={88}
      outerAlpha={0.2}
      innerScale={0.7}
      outerScale={1.5}
    /></div>
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
        <main><div>{children}</div></main>
        <footer
          style={{
            padding: `32px`,
            display:`flex`,
            alignItems:`center`,
            justifyContent: `center`
          }}
        >
          <h6 style={{margin:`0`}}>FORGA.IO © {new Date().getFullYear()}. BELGRADE, SERBIA - EUROPE</h6>
        </footer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
