import React, { useEffect, useState } from 'react'
import PropTypes from "prop-types"
import styled from "styled-components"
import { AnchorLink } from "gatsby-plugin-anchor-links";

import DarkModeToggle from "./toggler"
import Logo from "../images/logo.svg"

const HeaderWrapper = styled.label`

    .header-left {
      float:left;
    }

    .header-right {
      display:flex;
      align-items:center;
      float:right;
      cursor:none;
      
      a {
        padding:16px;
        text-decoration: none;
        overflow:hidden;
      }

     .link-hover:hover:after{
        left: 50%;
        bottom: 10px;
        transform: scaleX(1) translate(-50%, -50%);
        transform-origin: bottom left;
      }

      .link-hover {
        position:relative;
      }
      
      .link-hover:after {
        background-color: var(--textTitle);
        content: '';
        position: absolute;
        margin: 0 auto;
        height: 2px;
        bottom: 10px;
        left: 50%;
        background-color: var(--textTitle);
        transform: scaleX(0) translate(-50%, -50%);
        transform-origin: bottom right;
        transition: transform 0.25s ease-out;
        overflow:hidden;
        width: 85%;
      }

      a:last-of-type {
        border: 1px solid var(--textTitle);
        box-sizing: border-box;
        border-radius: 32px;
        text-decoration: none;
      }

      a:last-of-type:hover{
        color:var(--bg);
        background: var(--textTitle);
        transition:.3s;
      }
    }
`

const Header = ({ siteTitle }) => {

  const [scroll, setScroll] = useState(false);
  useEffect(() => {
    if(window !== "undefined") {
    window.addEventListener("scroll", () => {
      setScroll(window.scrollY > 50);
    });
  }
  }, []);


  return (
    <header
      className={scroll ? "scrolled" : ""}
      style={{
        background: `transparent`,
        backdropFilter: `blur(0)`,
        paddingTop: `16px`,
        paddingBottom: `16px`,
        left: `0`,
        top: `0`,
        position: `fixed`,
        width: `100%`,
        zIndex: `99`
      }}
    >
      <HeaderWrapper
        style={{
          margin: `0 auto`,
          maxWidth: `-webkit-fill-available`,
          padding: `0 16px`,
          display: `flex`,
          flexFlow: `row wrap`,
          justifyContent: `space-between`,
          alignItems: `center`,
        }}
      >
        <div className="header-left">
          <AnchorLink
            to="/">

            <Logo className="logo" />

          </AnchorLink>
        </div>
        <div className="header-right">
          <AnchorLink
            to="/#our-work"
            className="link-hover mobile-hide"
          >

            WORK

          </AnchorLink>
          <AnchorLink
            to="/#our-services"
            className="link-hover mobile-hide"
          >

            SERVICES

          </AnchorLink>
          <AnchorLink
            to="/#about-us"
            className="link-hover mobile-hide"
          >

            ABOUT

          </AnchorLink>
          <AnchorLink
            to="/#contact-us"
            className="contact"
          >
            CONTACT

          </AnchorLink>
          <DarkModeToggle />
        </div>
      </HeaderWrapper>
    </header>
  )
}


Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
