import React from 'react'
import styled from 'styled-components'
import { ThemeToggler } from 'gatsby-plugin-dark-mode'

const Label = styled.label`
    background:transparent;
    border: 1px solid var(--textTitle);
    box-sizing: border-box;
    border-radius: 32px;
    position:relative;
    text-decoration: none;
    display:flex;
    align-items:center;
    margin-left:16px;
    height: 56px;
    width: 56px;
    cursor:none;
    :hover {
      color:var(--bg);
      background: var(--textTitle);
      transition:.3s;
        svg {
          margin: 0 auto;
          path{
            fill:var(--bg);
          }
        }
    }
    svg {
      margin: 0 auto;
    }
    input {
        position:absolute;
        height:100%;
        visibility:hidden;
        width:100%;
    }
`
const DarkModeToggle = () => {
    return (
        <ThemeToggler>
        {({ theme, toggleTheme }) => (
          <Label>
            <input
              type="checkbox"
              onChange={e => toggleTheme(e.target.checked ? 'dark' : 'light')}
              checked={theme === 'dark'}
              className="switcher"
            />{' '}
            {theme === 'dark' ? (
                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11 17C9.4087 17 7.88258 16.3679 6.75736 15.2426C5.63214 14.1174 5 12.5913 5 11C5 9.4087 5.63214 7.88258 6.75736 6.75736C7.88258 5.63214 9.4087 5 11 5C12.5913 5 14.1174 5.63214 15.2426 6.75736C16.3679 7.88258 17 9.4087 17 11C17 12.5913 16.3679 14.1174 15.2426 15.2426C14.1174 16.3679 12.5913 17 11 17ZM10 0H12V3H10V0ZM10 19H12V22H10V19ZM2.515 3.929L3.929 2.515L6.05 4.636L4.636 6.05L2.515 3.93V3.929ZM15.95 17.364L17.364 15.95L19.485 18.071L18.071 19.485L15.95 17.364ZM18.071 2.514L19.485 3.929L17.364 6.05L15.95 4.636L18.071 2.515V2.514ZM4.636 15.95L6.05 17.364L3.929 19.485L2.515 18.071L4.636 15.95V15.95ZM22 10V12H19V10H22ZM3 10V12H0V10H3Z" fill="white"/>
                </svg>
                // <img src={ImagePathDark} onMouseOver={e => e.currentTarget.src = ImagePath}  onMouseOut={e => e.currentTarget.src = ImagePathDark} style={{margin:`0 auto`}} className='LightSwitch' alt="Light mode" />
                ) : (
                // <img src={ImagePath} onMouseOver={e => e.currentTarget.src = ImagePathDark} onMouseOut={e => e.currentTarget.src = ImagePath}  style={{margin:`0 auto`}}  className='DarkSwitch'  alt="Dark mode" />
                <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M7.822 2.238C7.05219 3.90906 6.81303 5.77591 7.13672 7.58706C7.46041 9.39821 8.33141 11.0667 9.63238 12.3676C10.9333 13.6686 12.6018 14.5396 14.4129 14.8633C16.2241 15.187 18.0909 14.9478 19.762 14.178C18.768 18.654 14.775 22 10 22C4.477 22 0 17.523 0 12C0 7.225 3.346 3.232 7.822 2.238ZM16.164 2.291L17 2.5V3.5L16.164 3.709C15.8124 3.79693 15.4913 3.97875 15.235 4.23503C14.9788 4.4913 14.7969 4.8124 14.709 5.164L14.5 6H13.5L13.291 5.164C13.2031 4.8124 13.0212 4.4913 12.765 4.23503C12.5087 3.97875 12.1876 3.79693 11.836 3.709L11 3.5V2.5L11.836 2.291C12.1874 2.20291 12.5083 2.02102 12.7644 1.76475C13.0205 1.50849 13.2021 1.18748 13.29 0.836L13.5 0H14.5L14.709 0.836C14.7969 1.1876 14.9788 1.5087 15.235 1.76497C15.4913 2.02125 15.8124 2.20307 16.164 2.291ZM21.164 7.291L22 7.5V8.5L21.164 8.709C20.8124 8.79693 20.4913 8.97875 20.235 9.23503C19.9788 9.4913 19.7969 9.8124 19.709 10.164L19.5 11H18.5L18.291 10.164C18.2031 9.8124 18.0212 9.4913 17.765 9.23503C17.5087 8.97875 17.1876 8.79693 16.836 8.709L16 8.5V7.5L16.836 7.291C17.1876 7.20307 17.5087 7.02125 17.765 6.76497C18.0212 6.5087 18.2031 6.1876 18.291 5.836L18.5 5H19.5L19.709 5.836C19.7969 6.1876 19.9788 6.5087 20.235 6.76497C20.4913 7.02125 20.8124 7.20307 21.164 7.291Z" fill="#0E1620"/>
                </svg>

            )}
          </Label>
        )}
      </ThemeToggler>
    );
  };
  
export default DarkModeToggle