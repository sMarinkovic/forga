import React, { Component, useState, useRef, useEffect, useCallback } from "react";
import * as THREE from 'three';
import { Canvas } from '@react-three/fiber'
import { OrbitControls } from '@react-three/drei'
import { useScrollPercentage } from "react-scroll-percentage";
import { useSpring } from '@react-spring/core'
import { Scene } from './scene'



const CanvasControl = () => {

    const [{ fill }, set] = useSpring({ fill: '' }, [])
    return (

        <div style={{ position: `fixed`, left: `0`, top: `0`, transform: `translate(0,0%)`, width: `100%`, height: `100%` }}>
            <Canvas className="canvas" dpr={[1, 1]}  >
                <Scene setBg={set} />
                <OrbitControls enablePan={false} enableZoom={false} maxPolarAngle={Math.PI / 1.2} minPolarAngle={Math.PI / 1.2} />
            </Canvas>
        </div>
    )
}

export default CanvasControl

